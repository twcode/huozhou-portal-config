<?php
namespace SmartyConfig\Common;

class SmartyConfig
{
    public static function configDir()
    {
        $dirArray = array(
            S_ROOT.'vendor/twcode/huozhou-portal-config/src/SmartyConfig/Hz',
            S_ROOT.'vendor/twcode/huozhou-portal-config/src/SmartyConfig/Common',
            S_ROOT.'vendor/twcode/huozhou-portal-config/src/SmartyConfig/',
        );

        return $dirArray;
    }
}
